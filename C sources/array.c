#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "array.h"


double **create_array(int nrows, int ncols){
	double **a=malloc(nrows*sizeof(*a));
	if (a==NULL){
		printf("Failed memory allocation");
		exit(EXIT_FAILURE);}
	for (int i=0;i<nrows;i++){
		a[i]=malloc(ncols*sizeof(*a[i]));
		if (a[i]==NULL){
			printf("Failed memory allocation");
			exit(EXIT_FAILURE);}
	}
	return a;
}

void delete_array(double **a,int nrows){
	for (int i=0;i<nrows;i++){
		free(a[i]); 
	}
	free(a);
}

double **fill_array(int nrows,char *name,int column){	
	FILE *fp;
	char *line = NULL;
	size_t len = 0;
	char read;
	double x1,x2,x3,x4,x5,x6,x7,x8;
	int k=0;
	double **array;
	array=create_array(nrows,1);
	fp=fopen(name, "r");

	while ((read = getline(&line, &len, fp)) != -1) {
		sscanf(line,"%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf",&x1,&x2,&x3,&x4,&x5,&x6,&x7,&x8);
		switch (column){
			case 1:
				array[k][0]=x1;
				break;
			case 2:
				array[k][0]=x2;
				break;
			case 3:
				array[k][0]=x3;
				break;
			case 4:
				array[k][0]=x4;
				break;
			case 5:
				array[k][0]=x5;
				break;
			case 6:
				array[k][0]=x6;
				break;
			case 7:
				array[k][0]=x7;
				break;			
			case 8:
				array[k][0]=x8;
				break;	
		}
		k++;
	}
	fclose(fp);
	return array;
}