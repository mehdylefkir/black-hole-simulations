#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "array.h"
#include <string.h>
#include <stdbool.h>

double function(double x);
double theta_d(double alpha, double i);
double deg2rad(double angle);
double **rk4 (int n, double t_max, double impact);
double redshift(double r, double b, double i, double alpha);
double flux(double r, double F0);

void image(int size, int n, double b_max, double inclination, double r_min, double r_max, double F0, bool secondary);
void spectrum(double E0,double E_min, double E_max, double dE, int m, double inclination, double r_min, double r_max);
void simulate(int size, int n, double inclination, bool secondary);

int main(){
	/* Has to be compiled with the command :  gcc -I include array.c simulations.c -lm -o out-bin */
	int size=100,n=100;
	double inclin_arr[4]={10,30,60,80};
	double inclination;
	bool secondary=false;
	
	for (int i=0;i<4;i++){
		inclination=inclin_arr[i];
		printf("#  NOTE  # : Simulating black hole with inclination %2.1lf deg\n",inclination);
		simulate(size,n,inclination,secondary);
	}
	printf("#  NOTE  # : End of the simulations\n");

	return 0;
}

void simulate(int size, int n, double inclination, bool secondary){
	double b_max=100,r_max=30.,r_min=3.,F0=200.;
	double E0=6.4, E_max=8.,E_min=3.,dE=0.01;
	
	printf("<  INFO  > : Computes the radius, the redshift and the flux \n");
	printf("<  INFO  > : b_max=%3.lf  r_max=%2.lf  r_min=%1.lf\n",b_max,r_max,r_min);
	image(size,n,b_max,inclination,r_min,r_max,F0,secondary);
	printf("\n<  INFO  > : Computes the spectrum ");
	printf("\n<  INFO  > : E0=%2.1lf  E_max=%1.0lf  E_min=%1.0lf  dE=%lf\n",E0,E_min,E_max,dE);
	spectrum(E0,E_min,E_max,dE,size*size,inclination,r_min,r_max);
	printf("\n");
	
}

void spectrum(double E0, double E_min, double E_max, double dE, int m, double inclination, double r_min, double r_max){
	FILE *fp;
	double **array,**redshift,**radius,**b;
	double E_curr,E_curr_next,E,z,r;
	char str[256]="",data[256]="",angle[128];
	int N;
	
	sprintf(angle,"%.0lf",inclination);
	strcat(strcat(strcat(data,"data_"),angle),"deg.txt");
	strcat(strcat(strcat(str,"spectrum_"),angle),"deg.txt");
	
	fp=fopen(str,"w+");
	fprintf(fp,"#C code | i=%lf deg | E0=%lf | dE=%lf | r_min=%lf | r_max=%lf \n",inclination,E0,dE,r_min,r_max);
	fprintf(fp,"# energy | intensity \n");

	N=(int)(fabs(E_max-E_min+2*dE)/dE);
	array=create_array(N,1);
	printf("<  INFO  > : %d bins in energy\n",N);
	redshift=fill_array(m,data,7);
	radius=fill_array(m,data,5);
	b=fill_array(m,data,1);
	
	for (int i=0;i<N;i++){
		printf("\r<  STEP  > : %d/%d",i,N);
		fflush(stdout);
		
		E_curr=E_min+i*dE;
		E_curr_next=E_min+(i+1.)*dE;
		array[i][0]=0.;
		
		for (int j=1;j<m;j++){
			z=redshift[j][0];
			r=radius[j][0];
			E=E0/(1.+z);
			
			if ((E>E_curr)&&(E<E_curr_next)&&((r_min<r)&&(r<r_max)))
				{
				array[i][0]=array[i][0]+b[j][0]/(powf(r,2)*powf(1+z,3));
				}
		}
		fprintf(fp,"%lf\t%lf\n",E_curr,array[i][0]);
	}		
	printf("\r<  STEP  > : %d/%d",N,N);
	fflush(stdout);
	fclose(fp);
	delete_array(b,m);
	delete_array(radius,m);
	delete_array(redshift,m);
	delete_array(array,N);
	
}

void image(int size,int n, double b_max, double inclination, double r_min, double r_max, double F0,bool secondary){
	FILE *fp;
	double **buff_rt;
	double alpha,theta,b,r,r_newton,f,z,x,y;
	double h_b=b_max/(size-1), h_alpha=2*M_PI/(size-1);
	char angle[128], str[256]="";
	
	sprintf(angle,"%.0lf",inclination);
	strcat(strcat(strcat(str,"data_"),angle),"deg.txt");
	
	fp=fopen(str,"w");
	fprintf(fp,"#C code | n=%d | i=%lf deg | size=%d | b_max=%lf | r_min=%lf | r_max=%lf \n",n,inclination,size,b_max,r_min,r_max);
	fprintf(fp,"# b | alpha | x | y | radius | radius_newton | redshift | flux \n");
	
	for (int i=0;i<size;i++){
		b=i*h_b;
		printf("\r<  STEP  > : %d/%d",i,size);
		fflush(stdout);
		
		for (int j=0;j<(size);j++){
			alpha=j*h_alpha;
			if (secondary){theta=theta_d(alpha,deg2rad(inclination))+M_PI;}
			else {theta=theta_d(alpha,deg2rad(inclination));}
			buff_rt=rk4(n,theta,b);
			
			r=1./buff_rt[n-1][1];
			r_newton=b/sin(theta);
			x=b*cos(alpha);
			y=b*sin(alpha);
			
			if ((r_min<r) &&(r<r_max)){
				z=redshift(r,b,deg2rad(inclination),alpha);
				f=flux(r,F0)/pow(z+1,4);
				}
			else{
				f=z=0;}
			fprintf(fp,"%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n",b,alpha,x,y,r,r_newton,z,f);
			
			delete_array(buff_rt,n);
		}
	}
	printf("\r<  STEP  > : %d/%d",size,size);
	fflush(stdout);
	
	
}

double flux(double r, double F0){
	double x=r/3.;
	return F0*pow(x,-3)/(1.-1./(2.*x))*(1.- 1./sqrt(x)+sqrt(1./(8.*x))*log((sqrt(2*x)+1)/(sqrt(2*x)-1)*((sqrt(2)-1)/(sqrt(2)+1))));
}

double redshift(double r, double b, double i, double alpha){
	double b_c=3*sqrt(3)*0.5;
	double x=r/3.;
	return (-1+1/sqrt(1-1/(2*x))*(1+pow(2*x,-3./2.)*cos(i)*cos(alpha)*b/b_c));
}

double **rk4 (int n, double t_max, double impact){
	double **array;
	double k1[2],k2[2],k3[2],k4[2];
	double h=t_max/n;
	
	array=create_array(n,3);
	array[0][0]=0.,array[0][1]=0.;
	array[0][2]=1./impact;

	for (int i=1; i<n;i++){
		
		array[i][0]=h+array[i-1][0];
		
		k1[0]=h*array[i-1][2];
		k1[1]=h*function(array[i-1][1]);
		
		k2[0]=h*(array[i-1][2]+0.5*k1[0]);
		k2[1]=h*function(array[i-1][1]+0.5*k1[1]);
		
		k3[0]=h*(array[i-1][2]+0.5*k2[0]);
		k3[1]=h*function(array[i-1][1]+0.5*k2[1]);
		
		k4[0]=h*(array[i-1][2]+k3[0]);
		k4[1]=h*function(array[i-1][1]+k3[1]);
		
		array[i][1]=array[i-1][1]+(1./6.)*(k1[0] + 2*k2[0] + 2*k3[0] + k4[0]);
		array[i][2]=array[i-1][2]+(1./6.)*(k1[1] + 2*k2[1] + 2*k3[1] + k4[1]);
	}
	return array;
}

double theta_d(double alpha, double i){
return acos((-sin(alpha)*cos(i))/sqrt(1-(cos(alpha)*cos(alpha))*(cos(i)*cos(i))));}
	
double deg2rad(double angle){
	return angle*M_PI/180.;
}

double function(double x){
	return 3./2.*x*x-x;
}
