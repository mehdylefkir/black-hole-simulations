#ifndef ARRAY_H_INCLUDED
#define ARRAY_H_INCLUDED

double **create_array(int nrows, int ncols);
void delete_array(double **a,int nrows);
double **fill_array(int nrows,char *name,int column);

#endif