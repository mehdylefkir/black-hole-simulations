import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc
from matplotlib import rcParams
import glob
import re
import os
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import gridspec
import cmasher as cmr

rc('text', usetex=True)
rc('legend',fontsize=20)
rc('xtick',labelsize=20)
rc('ytick',labelsize=20)
rc('axes', labelsize=20)


def plot_redshift_classic(X,Y,Z,b_max,inclination,title_bool=False,secondary=False):
    ax_max=b_max*0.15
    levels=100
    vmax=np.max(Z)
    vmin=np.min(Z)
    cmap=cmr.eclipse
    string_sec="primaire"
    if secondary: string_sec="secondaire"
    
    fig,ax=plt.subplots(1,1,figsize=(16,9))
    ax.set_aspect('equal')
    ax.set_xlim(-ax_max*1.3,ax_max*1.3)
    ax.set_ylim(-ax_max*0.7,ax_max*0.8)
    ax.set_xlabel(r'$x_{det} (r_s)$')
    ax.set_ylabel(r'$y_{det} (r_s)$')
    cnt=ax.contourf(X,Y,Z.T,levels=levels,cmap=cmap,antialiased=False)
        
    for c in cnt.collections:
        c.set_edgecolor("face")
        
    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='2.5%', pad=0.25)
    colmap = plt.cm.ScalarMappable(cmap=cmap)
    colmap.set_array(Z)
    colmap.set_clim(0., vmax)
    cbar=fig.colorbar(colmap,cax=cax, orientation='vertical', boundaries=np.linspace(vmin, vmax, 10))
    cbar.ax.get_yaxis().labelpad = 40
    cbar.ax.set_ylabel(r'Redshift $z$', rotation=270)
    if title_bool:
        ax.set_title(r"Redshift "+string_sec+"d'un trou noir à i="+str(int(inclination))+r"$^\circ$",fontsize=25)
    fig.tight_layout()
    fig.savefig('redshift_'+string_sec+str(int(inclination))+"deg.pdf",dpi=500)

def plot_flux_classic(X,Y,F,b_max,inclination,title_bool=False,secondary=False):
    ax_max=b_max*0.15
    levels=100
    vmax=np.max(F)
    cmap=cmr.ember
    string_sec="primaire"
    if secondary: string_sec="secondaire"
        
    fig,ax=plt.subplots(1,1,figsize=(16,9))
    ax.set_aspect('equal')
    ax.set_xlim(-ax_max*1.3,ax_max*1.3)
    ax.set_ylim(-ax_max*0.7,ax_max*0.8)
    ax.set_xlabel(r'$x_{det} (r_s)$')
    ax.set_ylabel(r'$y_{det} (r_s)$')
    cnt=ax.contourf(X,Y,F.T,levels=levels,cmap=cmap,antialiased=False)
        
    for c in cnt.collections:
        c.set_edgecolor("face")
        
    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='2.5%', pad=0.25)
    colmap = plt.cm.ScalarMappable(cmap=cmap)
    colmap.set_array(F)
    colmap.set_clim(0., vmax)
    cbar=fig.colorbar(colmap,cax=cax, orientation='vertical', boundaries=np.linspace(0, vmax, 10))
    cbar.ax.get_yaxis().labelpad = 40
    cbar.ax.set_ylabel(r'Flux kev/cm$^2$/s', rotation=270)
    if title_bool:
        ax.set_title(r"Image "+string_sec+" d'un trou noir à i="+str(int(inclination))+r"$^\circ$",fontsize=25)
    fig.tight_layout()
    fig.savefig('image_'+string_sec+str(int(inclination))+"deg.pdf",dpi=500)

def plot_isoradials_classic(X,Y,Z,b_max,inclination,levels=100,Z2=np.zeros(1),title_bool=False,secondary=False):
    cmap = cmr.ember
    vmax=0.75*b_max
    ax=[]
    string_sec=" primaire"
    if secondary: 
        string_sec=" image secondaire"
        a=2
        levels=levels*3
    else : a=1
    if not (Z2==0).all():
        n,m,widths=3,2,[5,0.1]
        fig=plt.figure(figsize=(8.27*1.5,11.69*1.5))
    else :
        n,m,widths=2,1,[15,0.5]
        fig=plt.figure(figsize=(8.27*1.5,11.69*0.9))
        
    gs =fig.add_gridspec(ncols=2, nrows=n, width_ratios=widths)
    
    ax.append(plt.subplot(gs[0,:],aspect=1.0))
    ax.append(plt.subplot(gs[1,0],aspect=1.0))
    
    #Newton approx plots
    if not (Z2==0).all():
        ax[0]=plt.subplot(gs[0,0],aspect=1.0)
        ax[1]=plt.subplot(gs[1,:],aspect=1.0)
        ax.append(plt.subplot(gs[2,0],aspect='equal'))
        ax[0].contourf(X,Y,Z2,vmin=0,vmax=vmax,cmap=cmap,levels=levels,linestyles="solid")
        ax[0].set_title("Isorayons dans l'approximation Newtonienne",fontsize=18)
        
        divider = make_axes_locatable(ax[0])
        cax = divider.append_axes('right', size='2.5%', pad=0.25)
        colmap1 = plt.cm.ScalarMappable(cmap=cmap)
        colmap1.set_array(Z2)
        colmap1.set_clim(0., vmax)
        cbar1=fig.colorbar(colmap1,cax=cax, orientation='vertical', boundaries=np.linspace(0, vmax, 10))
        cbar1.ax.get_yaxis().labelpad = 40
        cbar1.ax.set_ylabel(r'isorayon ($r_s$)', rotation=270)
    
    for axis in ax:
        axis.set_xlim(-vmax/a,vmax/a)
        axis.set_ylim(-vmax/(a*2),vmax/(2*a))
        axis.set_xlabel(r'$x_{det} (r_s)$')
        axis.set_ylabel(r'$y_{det} (r_s)$')
        
    isorad=ax[m-1].contour(X,Y,Z,vmin=0,vmax=vmax,cmap=cmap,levels=levels,linestyles="solid")
    ax[m-1].clabel(isorad, inline=True, fontsize=20,fmt='%1d ',colors="black",use_clabeltext=1)
    ax[m-1].set_title("Isorayons avec les géodésiques",fontsize=18)
    
    ax[m].contourf(X,Y,Z,vmin=0,vmax=vmax,levels=levels,cmap=cmap,linestyles="solid")
    divider = make_axes_locatable(ax[m])
    cax = divider.append_axes('right', size='2.5%', pad=0.25)
    colmap = plt.cm.ScalarMappable(cmap=cmap)
    colmap.set_array(Z)
    colmap.set_clim(0., vmax)
    cbar=fig.colorbar(colmap,cax=cax, orientation='vertical', boundaries=np.linspace(0, vmax, 10))
    cbar.ax.get_yaxis().labelpad = 40
    cbar.ax.set_ylabel(r'isorayon ($r_s$)', rotation=270)
    plt.grid()
    fig.tight_layout(rect=[0, 0,1., 1])    
    
    if title_bool:
        plt.subplots_adjust(left=None, bottom=None, right=None,top=0.85, wspace=None, hspace=0.3)
        fig.suptitle(r"Courbes isorayons"+string_sec+" observées sur le détecteur pour i="+str(int(inclination))+"$^\circ$",fontsize=25)
        fig.tight_layout(rect=[0, 0, 1, .95])
    
    fig.savefig("Isoradials_"+string_sec+str(int(inclination))+"deg.pdf")

def plot_spectrum_classic(spect_files):
    fig=plt.figure(figsize=(10,10))
    for file in spect_files:
        fp = open(file, "r")
        first=fp.readline()
        E0=np.round(float(re.findall( r'E0=(.*?) ', first)[0]),2)
        dE=np.round(float(re.findall( r'dE=(.*?) ', first)[0]),2)
        inclination=float(re.findall( r'i=(.*?) ', first)[0])
        r_min=float(re.findall( r'r_min=(.*?) ', first)[0])
        Spec=np.loadtxt(file)
        plt.step(Spec[:,0],Spec[:,1],label=r"$i=$"+str(int(inclination))+r"$^\circ$")
        fp.close()

    plt.xlim(np.min(Spec[:,0]),np.max(Spec[:,0]))
    plt.xlabel(r"Energy (keV)",fontsize=14)
    plt.ylabel(r"Photon/cm$^2$",fontsize=14)
    plt.title(r"Relativistic spectrum for $\Delta E$=${0}$".format(dE)+r"$\,$keV and $r_{min}=$"+r"${0}$".format(int(r_min))+r'$\,r_s$',fontsize=18)
    plt.legend()
    fig.savefig("Spectrum.pdf")

for file in glob.glob("data_*.txt"):
    fp = open(file, "r")
    print("<  INFO  > : Opening file : "+file)
    first=fp.readline()
    second=fp.readline()
    b_max=float(re.findall( r'b_max=(.*?) ', first)[0])
    inclination=float(re.findall( r'i=(.*?) ', first)[0])
    size=int(re.findall( r'size=(.*?) ', first)[0])
    secondary_bool=int(re.findall( r'secondary=(.*?) ', first)[0])

    Array=np.loadtxt(file)
    Array=Array[np.lexsort((Array[0:,1], Array[0:,0]))]
    hdr=first.replace("#","",2).replace("\n","",2)+'\n'+second.replace("#","",2).replace("\n","",2)
    np.savetxt(file,np.nan_to_num(Array,0),header=hdr.replace("  ",'',5),fmt="%.8g")

    alpha_list=Array[:,1][:size]
    b_list=np.unique(Array[:,0])
    B,A=np.meshgrid(b_list,alpha_list)
    X,Y=B*np.cos(A),B*np.sin(A)
    Z=np.reshape(Array[:,6],(size,size))
    F=np.reshape(Array[:,7],(size,size))
    R=np.nan_to_num(np.reshape(Array[:,4],(size,size)),0)
    R_newton=np.nan_to_num(np.reshape(Array[:,5],(size,size)),0)

    title_bool=True
    plot_flux_classic(X,Y,F,b_max,inclination,title_bool,secondary_bool)
    plot_redshift_classic(X,Y,Z,b_max,inclination,title_bool,secondary_bool)
    plot_isoradials_classic(X,Y,R.T,b_max,inclination,levels=100,Z2=R_newton.T,title_bool=True,secondary=secondary_bool)
    fp.close()
     
spec_files=glob.glob('spectrum_*')

if spec_files:
    plot_spectrum_classic(spec_files)
