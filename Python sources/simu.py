import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc
from matplotlib import rcParams
import cmasher as cmr
import sys
import time
import cmasher as cmr
from mpl_toolkits.axes_grid1 import make_axes_locatable

rc('text', usetex=True)
plt.rc('legend',fontsize=20)
plt.rc('xtick',labelsize=20)
plt.rc('ytick',labelsize=20)
plt.rc('axes', labelsize=20)    

def image(size,n,inclination,b_max,r_min,r_max,F0):
    start=time.time()
    alpha_list,b_list=np.linspace(-np.pi/2,np.pi/2,int((size-4)/2)),np.linspace(0,b_max,size)
    inclin_angle=np.deg2rad(inclination)
    Array=np.zeros(7)
    i=0
    for b in b_list:
        theta_0,b_0,r_0=0,b,np.inf
        sys.stdout.write("\r"+"Processing : " + str(i)+"/"+str(size))       
        
        for alpha in alpha_list:
            theta=theta_d(alpha,inclin_angle)
            r,t,rp=coordinates(n,theta_0,theta,b_0,r_0)
            theta_0,r_0,b_0=t[-1],r[-1],rp[-1] 
            alpha_sym=symmetric_angle(alpha)
            x,y,x_sym,y_sym=b*np.cos(alpha),b*np.sin(alpha),b*np.cos(alpha_sym),b*np.sin(alpha_sym)
            if (r_0>r_min and r_0<r_max):
                z,z_sym=redshift(r_0,b,inclin_angle,alpha),redshift(r_0,b,inclin_angle,alpha_sym)
                f,f_sym=flux(F0,r_0)/((1+z)**4),flux(F0,r_0)/((1+z_sym)**4)
            else: z=f=f_sym=z_sym=0
            Array=np.vstack((np.vstack((Array,np.array([b,alpha,x,y,r_0,z,f]))),np.array([b,alpha_sym,x_sym,y_sym,r_0,z_sym,f_sym])))
        
        for alpha in [0,np.pi]:
            theta_0,b_0,r_0=0,b,np.inf
            theta=theta_d(alpha,inclin_angle)
            r,t,rp=coordinates(n,theta_0,theta,b_0,r_0)
            theta_0,r_0,b_0=t[-1],r[-1],rp[-1]
           
            if alpha==np.pi:
                alpha_sym=-alpha
                x,y,x_sym,y_sym=b*np.cos(alpha),b*np.sin(alpha),b*np.cos(alpha_sym),b*np.sin(alpha_sym)
                if (r_0>r_min and r_0<r_max):
                    z,z_sym=redshift(r_0,b,inclin_angle,alpha),redshift(r_0,b,inclin_angle,alpha_sym)
                    f,f_sym=flux(F0,r_0)/((1+z)**4),flux(F0,r_0)/((1+z_sym)**4)
                else: z=f=f_sym=z_sym=0
                Array=np.vstack((np.vstack((Array,np.array([b,alpha,x,y,r_0,z,f]))),np.array([b,alpha_sym,x_sym,y_sym,r_0,z_sym,f_sym])))
            else:
                x,y=b*np.cos(alpha),b*np.sin(alpha)
                if (r_0>r_min and r_0<r_max):
                    z=redshift(r_0,b,inclin_angle,alpha)
                    f=flux(F0,r_0)/((1+z)**4)
                else: f=z=0
                Array=np.vstack((np.vstack((Array,np.array([b,alpha,x,y,r_0,z,f]))),np.array([b,alpha,x,y,r_0,z,f])))
        sys.stdout.flush()
        i+=1
    end=time.time()
    Array=np.nan_to_num(Array[1:,:],0)
    Array=Array[np.lexsort((Array[:,1], Array[:,0]))]
    hdr="Python code | n={0} | i={1}| size={2}| b_max={3} | r_min={4} | r_max={5}".format(n,inclination,size,b_max,r_min,r_max)
    np.savetxt("data_"+str(np.round(inclination,2))+"deg.txt",Array, fmt='%.18e',header=hdr) 
    print('\rTime enlapsed : '+str(int((end-start)//60))+" min : "+str(int((end-start)%60))+' s')
    return Array

def coordinates(n,theta_0,theta_d,b,r_0):
    array=rungekutta4(theta_0,theta_d,r_0,b,n)
    return 1/array[:,1],array[:,0],1/array[:,2]

def theta_d(alpha,i):
    return np.arccos((-np.sin(alpha)*np.cos(i))/np.sqrt(1-(np.cos(alpha)**2)*(np.cos(i)**2)))

def function(v):
    return np.array([v[1],3/2*v[0]*v[0]-v[0]])

def rungekutta4(a, b,r_0,bimp,n):
    h = 1.*(b - a)/n
    t,v=np.array([a]),np.array([1/r_0,1/bimp])
    res=np.array([[a,1/r_0,1/bimp]])
    for i in range (1,n+1):
        t=np.append(t,t[i-1]+h)
        k1 = h*function(v)
        k2 = h*function(v+ 0.5*k1)
        k3 = h*function(v+ 0.5*k2)
        k4 = h*function(v + k3)
        v = v+ (1./6.)*(k1 + 2*k2 + 2*k3 + k4)
        res=np.append(res,np.array([[t[i],v[0],v[1]]]),0)
    return res
    
def redshift(r, b, i, alpha):
	b_c=3*np.sqrt(3)*0.5
	x=r/3.
	return (-1+1/np.sqrt(1-1/(2*x))*(1+(2*x)**(-3./2.)*np.cos(i)*np.cos(alpha)*b/b_c))

def flux(F0,r):
    x=r/3.
    return F0*x**(-3)/(1.-1./(2.*x))*(1.- 1./np.sqrt(x)+np.sqrt(1./(8.*x))*np.log((np.sqrt(2*x)+1)/(np.sqrt(2*x)-1)*((np.sqrt(2)-1)/(np.sqrt(2)+1))));
def symmetric_angle(angle):
    if angle>0:
        return np.pi-angle
    else:
        return -np.pi-angle
        
inclination=10
b_max=100
n=100
size=500
r_min,r_max=3,30
F0=200
Array=image(size,n,inclination,b_max,r_min,r_max,F0)