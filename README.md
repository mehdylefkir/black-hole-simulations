# Black hole simulations

A simple simulation of a Schwarzschild black hole.

- Folder _C sources_ contains the C code simulating the black hole with an optimization (simulation.c) and without the optimization (simulation_origin.c).
    ` gcc -Wall -I  include array.c simulation.c -lm -o out-bin && ./out-bin`
- Folder _Python sources_ contains the _Python code simulating the black hole with an optimization.
    `python3 simu.py`
- Folder _code Pierre_ contains the code written by Pierre BARTOLI in C and somes examples of the Gnuplots commands to plot the results.
    ` gcc TrouNoir_BARTOLI_Pierre.c -o out-bin && ./out-bin`
- Folder _plots_ contains some snippets of code to plot the images for the report.
   


- Finally the script plotting.py is used to plot the results of the codes of _C sources_ and _Python sources_ and needs an X-server running to plot the images. It also needs the module [CMasher](https://github.com/1313e/CMasher) to plot the images with better colormaps.
