import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc
from matplotlib import rcParams
import glob
import re
import os
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import gridspec
import cmasher as cmr
import shutil

rc('text', usetex=True)
rc('legend',fontsize=20)
rc('xtick',labelsize=20)
rc('ytick',labelsize=20)
rc('axes', labelsize=20)

fig = plt.figure(figsize=(8.27*2,11.69/3*2.25),constrained_layout=False)
gs =fig.add_gridspec(ncols=2, nrows=2)
#gs.update(wspace=1, hspace=0.01) 
ax=[]
cmap=cmr.ember
levels=75
ax.append(plt.subplot(gs[0,0],aspect=1.0))
ax.append(plt.subplot(gs[1,0],aspect=1.0))
ax.append(plt.subplot(gs[0,1],aspect=1.0))
ax.append(plt.subplot(gs[1,1],aspect=1.0))

for (axis,file) in zip(ax,glob.glob("data_primary*.txt")):   
    #LOADING THE FILE
    fp = open(file, "r")
    print("<  INFO  > : Opening file : "+file)
    first=fp.readline()
    second=fp.readline()
    b_max=float(re.findall( r'b_max=(.*?) ', first)[0])
    inclination=float(re.findall( r'i=(.*?) ', first)[0])
    size=int(re.findall( r'size=(.*?) ', first)[0])
    secondary_bool=int(re.findall( r'secondary=(.*?) ', first)[0])
    Array=np.loadtxt(file)
    Array=Array[np.lexsort((Array[0:,1], Array[0:,0]))]
    #
    alpha_list=Array[:,1][:size]
    b_list=np.unique(Array[:,0])
    B,A=np.meshgrid(b_list,alpha_list)
    X,Y=B*np.cos(A),B*np.sin(A)
    Z=np.reshape(Array[:,6],(size,size))
    F=np.reshape(Array[:,7],(size,size))
    R=np.nan_to_num(np.reshape(Array[:,4],(size,size)),0)
    R_newton=np.nan_to_num(np.reshape(Array[:,5],(size,size)),0)
    R=F
    vmax=np.max(R)
    vmin=0
    
    ax_max=b_max*0.15
    levels=100
    axis.set_aspect('equal')
    axis.set_xlim(-ax_max*1.3,ax_max*1.3)
    axis.set_ylim(-ax_max*0.7,ax_max*0.8)
    axis.set_xlabel(r'$x_{det} (r_s)$')
    axis.set_ylabel(r'$y_{det} (r_s)$')
    axis.set_title(r"Image pour $i={0}^\circ$".format(str(int(inclination))),fontsize=18)
    cnt=axis.contourf(X,Y,R.T,levels=levels,cmap=cmap,antialiased=1)
    
    for c in cnt.collections:
        c.set_edgecolor("face")
        
    divider = make_axes_locatable(axis)
    cax = divider.append_axes('right', size='2.5%', pad=0.15)
    colmap = plt.cm.ScalarMappable(cmap=cmap)
    colmap.set_array(R.T)
    colmap.set_clim(vmin, vmax)
    cbar=fig.colorbar(colmap,cax=cax, orientation='vertical', boundaries=np.linspace(vmin, vmax, 10))
    cbar.ax.get_yaxis().labelpad = 20
    cbar.ax.set_ylabel(r'Flux (unité arbitraire)', rotation=270)
    plt.grid()
    fp.close()
fig.tight_layout()
fig.savefig("flux.pdf",dpi=500)