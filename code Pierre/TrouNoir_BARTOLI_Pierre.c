#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

double redshiftCalcul(double r,double b, double i,double alpha){

	double b_c = 3*sqrt(3)*0.5;
	double x= r/3.;
	return (-1 + 1/sqrt(1 - 1/(2*x))*(1 + pow(2*x, -3./2.)*cos(i)*cos(alpha)*b/b_c));
}

double fluxCalcul(double r,double F0){

	double x=r/3.;
	return F0*pow(x,-3)/(1.-1./(2.*x))*(1.- 1./sqrt(x)+sqrt(1./(8.*x))*log((sqrt(2*x)+1)/(sqrt(2*x)-1)*((sqrt(2)-1)/(sqrt(2)+1))));
}

double radVersDeg(double angle){

	return (angle/180) * M_PI;

}

double FonctionRK4(double x){

	double Y[2];
	return (3./2)*(x*x) - x;

}

void RK4Simple(double *k1Y, double *k2Y, double *k3Y, double *k4Y, double *y, double h){

	k1Y[0] = h*y[1];
	k1Y[1] = h*FonctionRK4(y[0]);
	k2Y[0] = h*(y[1] + 1./2*k1Y[0]);
	k2Y[1] = h*FonctionRK4(y[0] + 1./2*k1Y[1]);
	k3Y[0] = h*(y[1] + 1./2*k2Y[0]);
	k3Y[1] = h*FonctionRK4(y[0] + 1./2*k2Y[1]);
	k4Y[0] = h*(y[1] + k3Y[0]);
	k4Y[1] = h*FonctionRK4(y[0] + k3Y[1]);

	y[0] = y[0] + 1./6*(k1Y[0] + 2*k2Y[0] + 2*k3Y[0] + k4Y[0]);
	y[1] = y[1] + 1./6*(k1Y[1] + 2*k2Y[1] + 2*k3Y[1] + k4Y[1]);

}

void luminosite(double i, int nAlpha, int n, int nb, double rMax){

	//Creation et ouverture du fichier pour �crire les donn�es
	FILE *fp;
	fp = fopen("luminosite.txt", "w+");

	//Creation et ouverture du fichier pour �crire les donn�es temporaires
	FILE *tmp;
	tmp = fopen("tmp3.txt", "w+");

	//Initialization des pas
	double hAlpha = M_PI/(nAlpha-1);
	double hb = (100 - 0.01)/(nb-1);
	double k1Y[2], k2Y[2], k3Y[2], k4Y[2];

	//Initialisation du vecteur 2x1 pour contenir la valeur de u = 1/r
	double *U;
	U = malloc(2 * sizeof *U);

	/*L'id�e est de boucler sur les b et de remarquer que thetad est une fonction croissante de alpha pour
		 * un b donn� donc aller de 0 � thetad pour la premi�re it�ration et ensuite aller de thetad-1 � thetad
		 * avec le m�me pas donc r�duire le nombre de points*/
	for (int j = 0; j < nb; j++){

		printf("Iteration n : %d sur %d\n", j, (2*nb) - 1);

		//on par de 0.01 jusqu'a 100 pour b
		double b = 0.01 + hb*j;

		//Initialisation de theta, u et u'
		double theta = 0;
		U[0] = 0;
		U[1] = 1./b;

		//on boucle sur les alphas
		for (int a = 0; a < nAlpha; a++){

			//deux alpha, un du calcul de pr�sent et un du calcul pr�c�dent
			double alpha = -(M_PI/2) + hAlpha*a;
			double alpha_1 = -(M_PI/2) + hAlpha*(a-1);

			//thetad pr�sent et le pas original qui va de 0 � thetad
			double thetad = acos((-sin(alpha)*cos(i))/sqrt(1-cos(alpha)*cos(alpha)*cos(i)*cos(i)));
			double h = (thetad)/n;

			//Si la boucle n'est pas � l'it�ration premi�re alors on calcul le nouveau pas
			if(a > 0){

				//d'abord on calcul thetad-1
				double thetad_1 = acos((-sin(alpha_1)*cos(i))/sqrt(1-cos(alpha_1)*cos(alpha_1)*cos(i)*cos(i)));

				//Ensuite on regarde combien de point il faut pour aller de thetad-1 � thetad avec le pas original
				int nEcart = (thetad - thetad_1)/h;

				//si nEcart est 0 alors on fait quand m�me un pas
				if(nEcart == 0){
					nEcart = 1;
				}

				//ensuite on calcul le nouveau pas de thetad-1 � thetad
				double hReel = (thetad - thetad_1)/nEcart;

				//puis on calcul avec RK4 le U avec theta allant de thetad-1 � thetad
				for (int k = 0; k < nEcart; k++){

					theta = thetad_1 + k*hReel;
					RK4Simple(k1Y, k2Y, k3Y, k4Y, U, hReel);

				}

			}

			//si on est � la premi�re it�ration on calcul u normalement avec RK4 et theta allant de 0 � thetad
			else{

				for (int k = 0; k < n; k++){

					theta = k*h;

					RK4Simple(k1Y, k2Y, k3Y, k4Y, U, h);

				}
			}

			//on converti les coordonn�es en cartesien
			double X = b*cos(alpha);
			double Y = b*sin(alpha);

			//pour �viter que l'�criture et la lecture ne bug on �vite d'�crire les NaN et les inf
			if(1./U[0] > 3 && 1./U[0] < rMax){

				//calcul du redshift
				double z = redshiftCalcul(1./U[0], b, i, alpha);

				//valeur de de F0
				double F0 = 20;
				//Calcul du flux
				double Femis = fluxCalcul(1./U[0], F0);

				//Calcul de Fobs
				double Fobs = Femis/(pow(1+z, 4));

				fprintf(tmp, "%lf\t%lf\t%lf\t%lf\n", X, Y, 1./U[0], Fobs);
				fprintf(fp, "%lf\t%lf\t%lf\t%lf\n", X, Y, 1./U[0], Fobs);

			}
			//on y �crit 0
			else{
				//fprintf(fp, "%lf\t%lf\t%lf\t%lf\n", X, Y, 0., 0.);
				fprintf(tmp, "%lf\t%lf\t%lf\t%lf\n", 0., 0., 0., 0.);
			}



		}

		fprintf(tmp, "\n");
		fprintf(fp, "\n");

	}

	//initilasiation de la lecture
	double X, Y, r, Fobs;

	//on repart au debut du fichier pour le lire fichier temporaire
	fseek(tmp, 0, SEEK_SET);

	/**
	 * L'id�e est de lire le fichier temporaire et ecrire le sym�tique en recalculant Z et Fobs
	 */
	for (int j = 0; j < nb; j++){

		printf("Iteration n : %d sur %d\n", j + nb, (2*nb) - 1);
		double b = 0.01 + hb*j;

		for (int a = 0; a < nAlpha; a++){

			double alpha = (M_PI/2) + hAlpha*a;

			//on r�cup�re r
			fscanf(tmp, "%lf\t%lf\t%lf\t%lf", &X, &Y, &r, &Fobs);

			if(r > 3 && r < rMax){
				//calcul du redshfit avec le bon r
				double z = redshiftCalcul(r, b, i, alpha);

				//Calcul du flux
				double F0 = 20;
				double Femis = fluxCalcul(r, F0);
				double Fobs = Femis/(pow(1+z, 4));

				//puis on �crit le sym�trique avec -X
				fprintf(fp, "%lf\t%lf\t%lf\t%lf\n", -X, Y, r, Fobs);

			}

		}

		//saut de ligne
		fscanf(tmp, "");
		fprintf(fp, "\n");
	}

	//on ferme les fichier
	fclose(fp);
	fclose(tmp);
	remove("tmp3.txt");
	free(U);

}

void raies(double i, int nAlpha, int n, int nb, double rMax, double freq0, double I0){

	//Creation du fichier
	FILE *fp;
	fp = fopen("raies.txt", "w+");

	//Creation des pas
	double hAlpha = M_PI/(nAlpha-1);
	double hb = (100 - 0.0)/(nb-1);
	double k1Y[2], k2Y[2], k3Y[2], k4Y[2];

	//Creation du bin en fr�quence
	double freqMax = 200;
	double freqMin = 100;
	double df = 0.1;
	int nBin = (freqMax - freqMin)/df;
	double Is[nBin];

	//On initialise a z�ro
	for(int k = 0; k <= nBin; k++){
		Is[k] = 0;
	}

	//Initialisation du vecteur 2x1 pour contenir la valeur de u = 1/r
	double *U;
	U = malloc(2 * sizeof *U);

	/*L'id�e est de boucler sur les b et de remarquer que thetad est une fonction croissante de alpha pour
		 * un b donn� donc aller de 0 � thetad pour la premi�re it�ration et ensuite aller de thetad-1 � thetad
		 * avec le m�me pas donc r�duire le nombre de points*/
	for (int j = 0; j < nb; j++){

		printf("Iteration n : %d sur %d\n", j, nb - 1);

		//on par de 0.01 jusqu'a 100 pour b
		double b = 0.0 + hb*j;

		//Initialisation de theta, u et u'
		double theta = 0;
		U[0] = 0;
		U[1] = 1./b;

		//on boucle sur les alphas
		for (int a = 0; a < nAlpha; a++){

			//deux alpha, un du calcul de pr�sent et un du calcul pr�c�dent
			double alpha = -(M_PI/2) + hAlpha*a;
			double alpha_1 = -(M_PI/2) + hAlpha*(a-1);

			//thetad pr�sent et le pas original qui va de 0 � thetad
			double thetad = acos((-sin(alpha)*cos(i))/sqrt(1-cos(alpha)*cos(alpha)*cos(i)*cos(i)));
			double h = (thetad)/n;

			//Si la boucle n'est pas � l'it�ration premi�re alors on calcul le nouveau pas
			if(a > 0){

				//d'abord on calcul thetad-1
				double thetad_1 = acos((-sin(alpha_1)*cos(i))/sqrt(1-cos(alpha_1)*cos(alpha_1)*cos(i)*cos(i)));

				//Ensuite on regarde combien de point il faut pour aller de thetad-1 � thetad avec le pas original
				int nEcart = (thetad - thetad_1)/h;

				//si nEcart est 0 alors on fait quand m�me un pas
				if(nEcart == 0){
					nEcart = 1;
				}

				//ensuite on calcul le nouveau pas de thetad-1 � thetad
				double hReel = (thetad - thetad_1)/nEcart;

				//puis on calcul avec RK4 le U avec theta allant de thetad-1 � thetad
				for (int k = 0; k < nEcart; k++){

					theta = thetad_1 + k*hReel;
					RK4Simple(k1Y, k2Y, k3Y, k4Y, U, hReel);

				}

			}

			//si on est � la premi�re it�ration on calcul u normalement avec RK4 et theta allant de 0 � thetad
			else{

				for (int k = 0; k < n; k++){

					theta = k*h;

					RK4Simple(k1Y, k2Y, k3Y, k4Y, U, h);

				}
			}

			//pour �viter que l'�criture et la lecture ne bug on �vite d'�crire les NaN et les inf
			if(1./U[0] > 3 && 1./U[0] < rMax){

				//Calcul des cordonn�es
				double X = b*cos(alpha);
				double Y = b*sin(alpha);

				//Calcul du redshift
				double z = redshiftCalcul(1./U[0], b, i, alpha);

				//Calcul des intensit�e des raies � la fr�quence freq
				double Iobs = I0/(pow((1./U[0]),2)*(pow((z+1), 3)));
				double freq = freq0/(1+z);

				//Puis on calcul le alpha symtrique
				double alphaSym;

				if(alpha >= 0)
					alphaSym = (M_PI) - alpha;
				if(alpha <= 0)
					alphaSym = -M_PI - alpha;

				//le redshift sym�trique
				double zSym = redshiftCalcul(1./U[0], b, i, alphaSym);

				//L'intensit� sym�trique � la freqSym
				double IobsSym = I0/(pow((1./U[0]),2)*(pow((zSym+1), 3)));
				double freqSym = freq0/(1+zSym);

				//Puis on ajoute dans le bin
				for(int k = 0; k <= nBin; k++){

					double freqBin = freqMin + df*k;

					if(freq >= freqBin && freq <= (freqBin + df)){
							Is[k] += (Iobs)*b;
					}
					if(freqSym >= freqBin && freqSym <= (freqBin + df)){
							Is[k] += (IobsSym)*b;
					}
				}

			}
		}

	}

	//On �crit le bin dans un fichier pour faire les raies
	for(int k = 0; k < nBin; k++){

		double freqBin = freqMin + df*k;
		fprintf(fp, "%lf\t%lf\n", freqBin, Is[k]);

	}

	fclose(fp);
	free(U);

}

int main(){

	luminosite(radVersDeg(80), 1000, 1000, 1000, 30);
	//raies(radVersDeg(60), 2000, 2000, 2000, 30, 175, 1);

	return 0;

}
