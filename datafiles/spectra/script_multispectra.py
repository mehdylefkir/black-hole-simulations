import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc
from matplotlib import rcParams
import glob
import re
import os
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import gridspec
import cmasher as cmr
import shutil

rc('text', usetex=True)
rc('legend',fontsize=20)
rc('xtick',labelsize=20)
rc('ytick',labelsize=20)
rc('axes', labelsize=20)
fig = plt.figure(figsize=(8.27*1.5,11.69/3*4),constrained_layout=False)
gs =fig.add_gridspec(ncols=2, nrows=2)
gs.update(wspace=0.5, hspace=0.20) 
ax=[]
cmap=cmr.ember
levels=75
ax.append(plt.subplot(gs[0,0]))
ax.append(plt.subplot(gs[1,0]))
ax.append(plt.subplot(gs[0,1]))
ax.append(plt.subplot(gs[1,1]))

colorlist=["#ef3e36","#17bebb","#2e282a","#edb88b"]
for (axis,angle) in zip(ax,[10,30,60,80]):
    spectr="spectrum_"+str(angle)+"*.txt"
    for (col,file) in zip(colorlist,glob.glob(spectr)):   
        #LOADING THE FILE
        fp = open(file, "r")
        first=fp.readline()
        E0=np.round(float(re.findall( r'E0=(.*?) ', first)[0]),2)
        dE=np.round(float(re.findall( r'dE=(.*?) ', first)[0]),2)
        inclination=float(re.findall( r'i=(.*?) ', first)[0])
        r_min=float(re.findall( r'r_min=(.*?) ', first)[0])
        Spec=np.loadtxt(file)
        fp.close()
        axis.step(Spec[:,0],Spec[:,1],label=r"$r_\mathrm{min}"+r"={0}\,r_s$".format(str(int(r_min))),color=col)
        handles, labels = axis.get_legend_handles_labels()
        axis.legend(handles, labels)
    axis.set_xlim(np.min(Spec[:,0]),np.max(Spec[:,0]))
    axis.set_xlabel(r"Energie (keV)")
    axis.set_ylabel(r"Intensité (unité arbitraire)")
   
    axis.set_title(r"Spectres pour $i={0}^\circ$".format(angle),fontsize=24)
        
fig.tight_layout()
fig.savefig("spectra.pdf",dpi=500,bbox_inches='tight')